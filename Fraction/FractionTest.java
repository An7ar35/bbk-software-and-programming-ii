import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * The test class FractionTest.
 * 
 * @author Es Alwyn DAVISON <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 5 January 2014
 * @version 1.0
 */
public class FractionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link Fraction#Fraction(int, int)}.
	 */
	@Test
	public void testFractionIntInt() {
		Fraction fraction1 = new Fraction( 5, 10 );
		assertEquals( "1/2", fraction1.toString() );
		Fraction fraction2 = new Fraction( -5, 10 );
		assertEquals( "-1/2", fraction2.toString() );
		Fraction fraction3 = new Fraction( 5, -10 );
		assertEquals( "-1/2", fraction3.toString() );
		Fraction fraction4 = new Fraction( -5, -10 );
		assertEquals( "1/2", fraction4.toString() );
		Fraction fraction5 = new Fraction( 0, -1 );
		assertEquals( "0/1", fraction5.toString() );
		Fraction fraction6 = new Fraction( -0, 1 );
		assertEquals( "0/1", fraction6.toString() );
	}

	/**
	 * Test method for {@link Fraction#Fraction(int)}.
	 */
	@Test
	public void testFractionInt() {
		Fraction fraction1 = new Fraction( 2 );
		assertEquals( "2/1", fraction1.toString() );
		Fraction fraction2 = new Fraction( -2 );
		assertEquals( "-2/1", fraction2.toString() );
		Fraction fraction3 = new Fraction( 0 );
		assertEquals( "0/1", fraction3.toString() );
		Fraction fraction4 = new Fraction( -0 );
		assertEquals( "0/1", fraction4.toString() );
	}

	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test
	public void testFractionString() {
		Fraction fraction1 = new Fraction( "25/50" );
		assertEquals( "1/2", fraction1.toString() );
		Fraction fraction2 = new Fraction( "-25/50" );
		assertEquals( "-1/2", fraction2.toString() );
		Fraction fraction3 = new Fraction( "25/-50" );
		assertEquals( "-1/2", fraction3.toString() );
		Fraction fraction4 = new Fraction( "-25/-50" );
		assertEquals( "1/2", fraction4.toString() );
		Fraction fraction5 = new Fraction( " 25 / 50 " );
		assertEquals( "1/2", fraction5.toString() );
		Fraction fraction6 = new Fraction( "- 25 / - 50" );
		assertEquals( "1/2", fraction6.toString() );
		Fraction fraction7 = new Fraction( "25 / -50" );
		assertEquals( "-1/2", fraction7.toString() );
	}

	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = NumberFormatException.class )
	public void testFractionString_InvalidString_A() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( "1/99999999999" );
	}

	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = NumberFormatException.class )
	public void testFractionString_InvalidString_B() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( "99999999999/1" );
	}
	
	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = NumberFormatException.class )
	public void testFractionString_InvalidString_C() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( "25\50" );
	}
	
	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = NumberFormatException.class )
	public void testFractionString_InvalidString_D() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( "     " );
	}
	
	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = NumberFormatException.class )
	public void testFractionString_InvalidString_E() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( "25\5o" );
	}
	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = ArithmeticException.class )
	public void testFractionString_InvalidString_F() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( "9999999999/99" );
	}

	/**
	 * Test method for {@link Fraction#Fraction(java.lang.String)}.
	 */
	@Test( expected = IllegalArgumentException.class )
	public void testFractionString_InvalidString_G() {
		@SuppressWarnings( "unused" )
		Fraction fraction = new Fraction( " 2 5 / 0 " );
	}

	/**
	 * Test method for {@link Fraction#add(Fraction)}.
	 */
	@Test
	public void testAdd() {
		Fraction fraction1 = new Fraction( 5, 10 );
		Fraction fraction2 = new Fraction( 20, 40 );
		Fraction fraction3 = fraction1.add( fraction2 );
		assertEquals( "1/1", fraction3.toString() );
	}

	/**
	 * Test method for {@link Fraction#add(Fraction)}.
	 */
	@Test( expected = ArithmeticException.class )
	public void testAdd_Overflow() {
		Fraction fraction1 = new Fraction( 999999999, 2 );
		Fraction fraction2 = new Fraction( 999999999, 2 );
		fraction1.add( fraction2 );
	}

	/**
	 * Test method for {@link Fraction#subtract(Fraction)}.
	 */
	@Test
	public void testSubtract() {
		Fraction fraction1 = new Fraction( 20, 40 );
		Fraction fraction2 = new Fraction( 5, 20 );
		Fraction fraction3 = fraction1.subtract( fraction2 );
		assertEquals( "1/4", fraction3.toString() );
	}

	/**
	 * Test method for {@link Fraction#subtract(Fraction)}.
	 */
	@Test( expected = ArithmeticException.class )
	public void testSubtract_Overflow() {
		Fraction fraction1 = new Fraction( 5, 999999999 );
		Fraction fraction2 = new Fraction( 5, 2 );
		fraction1.subtract( fraction2 );
	}

	/**
	 * Test method for {@link Fraction#multiply(Fraction)}.
	 */
	@Test
	public void testMultiply() {
		Fraction fraction1 = new Fraction( 1, 2 );
		Fraction fraction2 = new Fraction( 60, 30 );
		Fraction fraction3 = fraction1.multiply( fraction2 );
		assertEquals( "1/1", fraction3.toString() );
	}

	/**
	 * Test method for {@link Fraction#multiply(Fraction)}.
	 */
	@Test( expected = ArithmeticException.class )
	public void testMultiply_Overflow() {
		Fraction fraction1 = new Fraction( 2, 999999999 );
		Fraction fraction2 = new Fraction( 2, 999999999 );
		fraction1.multiply( fraction2 );
	}

	/**
	 * Test method for {@link Fraction#divide(Fraction)}.
	 */
	@Test
	public void testDivide() {
		Fraction fraction1 = new Fraction( 1, 2 );
		Fraction fraction2 = new Fraction( 60, 30 );
		Fraction fraction3 = fraction1.divide( fraction2 );
		assertEquals( "1/4", fraction3.toString() );
	}

	/**
	 * Test method for {@link Fraction#divide(Fraction)}.
	 */
	@Test( expected = ArithmeticException.class )
	public void testDivide_Overflow() {
		Fraction fraction1 = new Fraction( 2, 999999999 );
		Fraction fraction2 = new Fraction( 999999999, 2 );
		fraction1.divide( fraction2 );
	}

	/**
	 * Test method for {@link Fraction#abs()}.
	 */
	@Test
	public void testAbs() {
		Fraction fraction1 = new Fraction( 2, 5 );
		Fraction fraction1abs = fraction1.abs();
		assertEquals( "2/5", fraction1abs.toString() );
		Fraction fraction2 = new Fraction( -2, 5 );
		Fraction fraction2abs = fraction2.abs();
		assertEquals( "2/5", fraction2abs.toString() );
	}

	/**
	 * Test method for {@link Fraction#negate()}.
	 */
	@Test
	public void testNegate() {
		Fraction fraction1 = new Fraction( 1, 1 );
		Fraction fraction1n = fraction1.negate();
		assertEquals( "-1/1", fraction1n.toString() );
		Fraction fraction2 = new Fraction( -1, -1 );
		Fraction fraction2n = fraction2.negate();
		assertEquals( "-1/1", fraction2n.toString() );
		Fraction fraction3 = new Fraction( 1, -1 );
		Fraction fraction3n = fraction3.negate();
		assertEquals( "1/1", fraction3n.toString() );
		Fraction fraction4 = new Fraction( -1, 1 );
		Fraction fraction4n = fraction4.negate();
		assertEquals( "1/1", fraction4n.toString() );
	}

	/**
	 * Test method for {@link Fraction#inverse()}.
	 */
	@Test
	public void testInverse() {
		Fraction fraction1 = new Fraction( 2, 5 );
		Fraction fraction1inv = fraction1.inverse();
		assertEquals( "5/2", fraction1inv.toString() );
		Fraction fraction2 = new Fraction( -2, 5 );
		Fraction fraction2inv = fraction2.inverse();
		assertEquals( "-5/2", fraction2inv.toString() );
	}

	/**
	 * Test method for {@link Fraction#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		Fraction fraction1 = new Fraction( 1, 2 );
		Fraction fraction2 = new Fraction( 1, 2 );
		Fraction fraction3 = new Fraction( 1, 3 );
		Object x = new Object();
		assertEquals( true, fraction1.equals( fraction2 ) );
		assertEquals( false, fraction1.equals( x ) );
		assertEquals( false, fraction1.equals( fraction3 ) );
	}

	/**
	 * Test method for {@link Fraction#greaterThan(Fraction)}.
	 */
	@Test
	public void testGreaterThan() {
		Fraction fraction1 = new Fraction( 1, 1 );
		Fraction fraction2 = new Fraction( 1, 2 );
		Fraction fraction3 = new Fraction( 1, 4 );
		assertEquals( true, fraction1.greaterThan( fraction2 ) );
		assertEquals( true, fraction1.greaterThan( fraction3 ) );
		assertEquals( false, fraction3.greaterThan( fraction2 ) );
		assertEquals( false, fraction3.greaterThan( fraction1 ) );
	}

	/**
	 * Test method for {@link Fraction#lessThan(Fraction)}.
	 */
	@Test
	public void testLessThan() {
		Fraction fraction1 = new Fraction( 1, 1 );
		Fraction fraction2 = new Fraction( 1, 2 );
		Fraction fraction3 = new Fraction( 1, 4 );
		assertEquals( true, fraction3.lessThan( fraction2 ) );
		assertEquals( true, fraction3.lessThan( fraction1 ) );
		assertEquals( false, fraction1.lessThan( fraction2 ) );
		assertEquals( false, fraction1.lessThan( fraction3 ) );
	}

}
