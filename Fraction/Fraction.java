/**
 * <b>Fraction class API</b><br/>
 * Enables the creation of fractions as objects and provides various
 * functionalities to them:<br/>
 * mathematical manipulation, boolean comparisons and string output
 * 
 * @author Es Alwyn DAVISON <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 5 January 2014
 * @version 0.9
 */
public class Fraction {
	private static final long INT_MAX_VALUE = Integer.MAX_VALUE;
	private static final long INT_MIN_VALUE = Integer.MIN_VALUE;
	private int n;
	private int d;

	/**
	 * Creates a fraction with a numerator and a denominator
	 * 
	 * @param n
	 *            numerator of the fraction
	 * @param d
	 *            denominator of the fraction
	 * @exception IllegalArgumentException
	 *                if denominator 'n' is equal to 0
	 */
	public Fraction( int n, int d ) throws IllegalArgumentException {
		if ( d == 0 ) {
			throw new IllegalArgumentException( "Denominator cannot be 0" );
		}
		normalise( n, d );
	}

	/**
	 * Creates a fraction with a numerator and a predefined denominator of value
	 * '1'
	 * 
	 * @param n
	 *            numerator of the fraction (Denominator is assumed to be 1 when
	 *            only one int is given)
	 */
	public Fraction( int n ) {
		this.n = n;
		this.d = 1;
		normalise( n, d );
	}

	/**
	 * Creates a fraction with a numerator and a denominator extracted from the
	 * given string <i> (Uses {@link java.util.regex.Matcher} to validate the
	 * input string)</i>
	 * 
	 * @param f
	 *            complete fraction represented as a string
	 * @exception NumberFormatException
	 *                if input is not formated as a fraction
	 * @exception NumberFormatException
	 *                if input number(s) are greater than a length of 10
	 * @exception ArithmeticException
	 *                if input number(s) are outside the range of an INT
	 * @exception IllegalArgumentException
	 *                if input denominator is equal to 0
	 */
	public Fraction( String f ) throws NumberFormatException, IllegalArgumentException {
		if ( f.matches( "(?ix)\\A[\\s]?[\\x2D]?[\\s]?[\\d]+[\\s]?[\\x2F][\\s]?[\\x2D]?[\\s]?[\\d]+[\\s]?\\z" ) != true ) {
			throw new NumberFormatException( "Invalid input" );
		}
		f = f.replace( " ", "" );
		String[] sFraction = f.split( "/" );
		if ( sFraction[ 0 ].length() > 10 || sFraction[ 1 ].length() > 10 ) {
			throw new NumberFormatException( "Number(s) in String too large for casting to INT" );
		}
		long nTemp = Long.parseLong( sFraction[ 0 ] );
		long dTemp = Long.parseLong( sFraction[ 1 ] );
		if ( nTemp > INT_MAX_VALUE || nTemp < INT_MIN_VALUE || dTemp > INT_MAX_VALUE || dTemp < INT_MIN_VALUE ) {
			throw new ArithmeticException( "Integer overflow" );
		}
		if ( dTemp == 0 ) {
			throw new IllegalArgumentException( "Denominator cannot be 0" );
		}
		int n = (int) nTemp;
		int d = (int) dTemp;
		normalise( n, d );
	}

	/**
	 * Adds this fraction to another given fraction
	 * 
	 * @param f
	 *            fraction to add
	 * @return the result as a Fraction object
	 * @exception ArithmeticException
	 *                if resulting nominator/denominator of calculation is
	 *                outside the range of an INT
	 */
	public Fraction add( Fraction f ) throws ArithmeticException {
		long n = (long) this.n * (long) f.d + (long) this.d * (long) f.n;
		long d = (long) this.d * (long) f.d;
		if ( n > INT_MAX_VALUE || n < INT_MIN_VALUE || d > INT_MAX_VALUE || d < INT_MIN_VALUE ) {
			throw new ArithmeticException( "Integer overflow" );
		}
		return new Fraction( (int) n, (int) d );
	}

	/**
	 * Subtracts this fraction by another given fraction
	 * 
	 * @param f
	 *            fraction to subtract
	 * @return the result as a Fraction object
	 * @exception ArithmeticException
	 *                if resulting nominator/denominator of calculation is
	 *                outside the range of an INT
	 */
	public Fraction subtract( Fraction f ) throws ArithmeticException {
		long n = (long) this.n * (long) f.d - (long) this.d * (long) f.n;
		long d = (long) this.d * (long) f.d;
		if ( n > INT_MAX_VALUE || n < INT_MIN_VALUE || d > INT_MAX_VALUE || d < INT_MIN_VALUE ) {
			throw new ArithmeticException( "Integer overflow" );
		}
		return new Fraction( (int) n, (int) d );
	}

	/**
	 * Multiplies this fraction with another given fraction
	 * 
	 * @param f
	 *            fraction to multiply with
	 * @return the result as a Fraction object
	 * @exception ArithmeticException
	 *                if resulting nominator/denominator of calculation is
	 *                outside the range of an INT
	 */
	public Fraction multiply( Fraction f ) throws ArithmeticException {
		long n = (long) this.n * (long) f.n;
		long d = (long) this.d * (long) f.d;
		if ( n > INT_MAX_VALUE || n < INT_MIN_VALUE || d > INT_MAX_VALUE || d < INT_MIN_VALUE ) {
			throw new ArithmeticException( "Integer overflow" );
		}
		return new Fraction( (int) n, (int) d );
	}

	/**
	 * Divides this fraction by another given fraction
	 * 
	 * @param f
	 *            fraction to divide with
	 * @return the result as a Fraction object
	 * @exception ArithmeticException
	 *                if resulting nominator/denominator of calculation is
	 *                outside the range of an INT
	 */
	public Fraction divide( Fraction f ) throws ArithmeticException {
		long n = (long) this.n * (long) f.d;
		long d = (long) this.d * (long) f.n;
		if ( n > INT_MAX_VALUE || n < INT_MIN_VALUE || d > INT_MAX_VALUE || d < INT_MIN_VALUE ) {
			throw new ArithmeticException( "Integer overflow" );
		}
		return new Fraction( (int) n, (int) d );
	}

	/**
	 * Returns the absolute value of this fraction as a new fraction
	 * 
	 * @return the result as a Fraction object
	 */
	public Fraction abs() {
		if ( this.n < 0 || this.d < 0 ) {
			Fraction f = this.negate();
			return f;
		} else {
			return this;
		}
	}

	/**
	 * Returns the negative of this fraction
	 * 
	 * @return the negated fraction as a Fraction object
	 */
	public Fraction negate() {
		int n = this.n * -1;
		int d = this.d;
		Fraction f = new Fraction( n, d );
		return f;
	}

	/**
	 * Returns the reverse of this fraction as a new fraction
	 * 
	 * @return the reversed fraction as a new fraction object
	 */
	public Fraction inverse() {
		int n = this.d;
		int d = this.n;
		Fraction f = new Fraction( n, d );
		return f;
	}

	/**
	 * Checks if object in parameter is the same as this fraction
	 * 
	 * @param o
	 *            object to check
	 * @return the result as a boolean
	 */
	public boolean equals( Object o ) {
		// if( o != null && o instanceof Fraction )....
		if ( o.getClass() == Fraction.class ) {
			return ( (Fraction) o ).n == this.n && ( (Fraction) o ).d == this.d;
		}
		return false;
	}

	/**
	 * Checks if this fraction is greater than given fraction
	 * 
	 * @param f
	 *            fraction to check against
	 * @return the result as a boolean
	 */
	public boolean greaterThan( Fraction f ) {
		return ( (long) this.n * (long) f.d > (long) this.d * (long) f.n );
	}

	/**
	 * Checks if this fraction is smaller than given fraction
	 * 
	 * @param f
	 *            fraction to check against
	 * @return the result as a boolean
	 */
	public boolean lessThan( Fraction f ) {
		return ( (long) f.n * (long) this.d > (long) f.d * (long) this.n );
	}

	/**
	 * Converts this fraction into a string
	 * 
	 * @return the "numerator/denominator" as a string
	 */
	public String toString() {
		return this.n + "/" + this.d;
	}

	/**
	 * Normalises a fraction using an Euclidean algorithm (division).
	 * 
	 * @param n
	 *            numerator of fraction to be normalised
	 * @param d
	 *            denominator of fraction to be normalised
	 */
	private void normalise( int n, int d ) {
		// check for negatives and hold flag until the end of normalise
		boolean negative = false;
		if ( n < 0 && d > 0 ) {
			n *= -1;
			negative = true;
		}
		if ( d < 0 && n >= 0 ) {
			d *= -1;
			negative = true;
		}
		// normalisation
		if ( n == 0 ) {
			this.n = 0;
			this.d = 1;
		} else {
			int a = n;
			int b = d;
			int t = 0;
			while ( b != 0 ) {
				t = b;
				b = a % t;
				a = t;
			}
			this.n = n / a;
			this.d = d / a;
			if ( negative == true ) {
				this.n *= -1;
			}
		}
	}
}