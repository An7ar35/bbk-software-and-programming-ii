# README #

Assignments made as part of the Software and Programming II module 2013 at Birkbeck University.

##Dominoes##
1v1/1vPC Domino game
(Group project: Main game code by E.A.Davison, Unit Tests by F. Cardinali)

##Fraction##
Library class to deal with Fractions

##NumericSpellChecker##
Spell Checker class whose “dictionary” is a list of numbers that returns the best match(s).