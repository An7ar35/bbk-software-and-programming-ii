import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 */

/**
 * The test class NumericSpellCheckerTest.
 * 
 * @author Es Alwyn DAVISON <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 5 January 2014
 * @version 0.5
 */
public class NumericSpellCheckerTest {
	private NumericSpellChecker numericSpellCheck;
	private int[] randomDict = { 5, 0, 111, 2222, 33333, -555555, 123456, -111, 10 };

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		numericSpellCheck = new NumericSpellChecker();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link NumericSpellChecker#generateRandomArray(int)}.
	 */
	@Test
	public void testGenerateRandomArray() {
		int[] array1 = NumericSpellChecker.generateRandomArray( 10 );
		assertEquals( true, array1.length == 10 );
	}

	/**
	 * Test method for {@link NumericSpellChecker#createDictionary(int[])}.
	 */
	@Test
	public void testCreateDictionary() {
		numericSpellCheck.createDictionary( randomDict );
		randomDict[ 0 ] = 6;
		assertEquals( true, numericSpellCheck.dictionary[ 0 ] == 5 );
	}

	/**
	 * Test method for {@link NumericSpellChecker#createDictionary(int[])}.
	 */
	@Test( expected = NullPointerException.class )
	public void testCreateDictionary_EmptySeed() {
		int[] emptyArray = new int[ 0 ];
		numericSpellCheck.createDictionary( emptyArray );
	}

	/**
	 * Test method for {@link NumericSpellChecker#createRandomDictionary(int)}.
	 */
	@Test
	public void testCreateRandomDictionary() {
		numericSpellCheck.createRandomDictionary( 666 );
		assertEquals( true, numericSpellCheck.dictionary.length == 666 );
	}

	/**
	 * Test method for {@link NumericSpellChecker#exactMatch(int)}.
	 */
	@Test
	public void testExactMatch() {
		numericSpellCheck.dictionary = randomDict;
		assertEquals( true, numericSpellCheck.exactMatch( 123456 ) );
		assertEquals( true, numericSpellCheck.exactMatch( -111 ) );
		assertEquals( true, numericSpellCheck.exactMatch( 0 ) );
		assertEquals( false, numericSpellCheck.exactMatch( -123456 ) );
	}

	/**
	 * Test method for {@link NumericSpellChecker#countDifferences(int, int)}.
	 */
	@Test
	public void testCountDifferences() {
		assertEquals( true, NumericSpellChecker.countDifferences( 123456, 123456 ) == 0 );
		assertEquals( true, NumericSpellChecker.countDifferences( 123456, -123456 ) == 1 );
		assertEquals( true, NumericSpellChecker.countDifferences( 123499, 123456 ) == 2 );
		assertEquals( true, NumericSpellChecker.countDifferences( 193959, -123456 ) == 4 );
		assertEquals( true, NumericSpellChecker.countDifferences( 11, 11111111 ) == 6 );
		assertEquals( true, NumericSpellChecker.countDifferences( 11, -11111111 ) == 7 );
		assertEquals( true, NumericSpellChecker.countDifferences( 999999999, 0 ) == 9 );
	}

	/**
	 * Test method for {@link NumericSpellChecker#findBestMatches(int)}.
	 */
	@Test
	public void testFindBestMatches() {
		// {5, 0, 111, 2222, 33333, -555555, 123456, -111, 10 };
		numericSpellCheck.dictionary = randomDict;
		int[] result = numericSpellCheck.findBestMatches( 33303 );
		assertEquals( true, result[ 0 ] == 33333 );
		result = numericSpellCheck.findBestMatches( 1 );
		assertEquals( true, result[ 0 ] == 5 && result[ 1 ] == 0 );
		result = numericSpellCheck.findBestMatches( 555555 );
		assertEquals( true, result[ 0 ] == -555555 );
		result = numericSpellCheck.findBestMatches( -111 );
		assertEquals( true, result[ 0 ] == -111 );
		result = numericSpellCheck.findBestMatches( 0 );
		assertEquals( true, result[ 0 ] == 0 );
	}

	/**
	 * Test method for {@link NumericSpellChecker#findBestMatches(int)}.
	 */
	@Test( expected = NullPointerException.class )
	public void testFindBestMatches_EmptySeed() {
		int[] emptyArray = new int[ 0 ];
		numericSpellCheck.dictionary = emptyArray;
		numericSpellCheck.findBestMatches( 666 );
	}
}
