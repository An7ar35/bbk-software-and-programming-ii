import java.util.Random;
import java.util.Scanner;
import java.util.Comparator;
import java.util.Arrays;

/**
 * <b>NumericSpellChecker</b><br/>
 * 
 * @author Es Alwyn DAVISON <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 5 January 2014
 * @version 0.1b
 */
public class NumericSpellChecker {
	public int[] dictionary;

	/**
	 * Generates an array of random integer of specified size
	 * 
	 * @param size
	 *            size of the array to be created
	 * @return the integer array
	 */
	public static int[] generateRandomArray( int size ) {
		int[] array = new int[ size ];
		Random random = new Random();
		for ( int i = 0; i < size; ++i ) {
			array[ i ] = random.nextInt();
		}
		return array;
	}

	/**
	 * Creates the {@link #dictionary dictionary} array cloned from a given
	 * array
	 * 
	 * @param numbers
	 *            array to be used to populate the dictionary
	 * @exception nullPointerException
	 *                when the {@link #dictionary dictionary} is empty
	 */
	public void createDictionary( int[] numbers ) throws NullPointerException {
		if ( numbers == null || numbers.length < 1 ) {
			throw new NullPointerException( "Array in argument is empty" );
		}
		dictionary = (int[]) numbers.clone();
	}

	/**
	 * Sets up the dictionary by calling {@link #createDictionary(int[])
	 * createDictionary} and {@link #generateRandomArray(int)
	 * generateRandomArray} methods
	 * 
	 * @param size
	 *            size of the dictionary (i.e.: number of integers to be created
	 *            inside it)
	 */
	public void createRandomDictionary( int size ) {
		createDictionary( generateRandomArray( size ) );
	}

	/**
	 * Checks if there is an exact match in the dictionary for the given integer
	 * 
	 * @param inputNumber
	 *            integer to check for a match
	 * @return whether a match exists or not in the form of a boolean
	 */
	public boolean exactMatch( int inputNumber ) {
		for ( int x : this.dictionary ) {
			if ( inputNumber == x ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks the amount of differences between two integers
	 * 
	 * @param a
	 *            first integer
	 * @param b
	 *            second integer
	 * @return the number of differences as an integer
	 */
	public static int countDifferences( int a, int b ) {
		int diffCounter = 0;
		if ( a == 0 || b == 0 ) {
			if ( a == 0 ) {
				int temp = a;
				a = b;
				b = temp;
			}
			if ( a < 0 ) {
				diffCounter++;
				a *= -1;
			}
			if ( a % 10 != 0 ) {
				diffCounter++;
			}
			a /= 10;
			while ( a > 0 ) {
				diffCounter++;
				a /= 10;
			}
		} else {
			if ( a < 0 && b > 0 ) {
				diffCounter++;
				a *= -1;
			} else if ( b < 0 && a > 0 ) {
				diffCounter++;
				b *= -1;
			} else if ( a < 0 && b < 0 ) {
				a *= -1;
				b *= -1;
			}
			while ( a > 0 && b > 0 ) {
				if ( a % 10 != b % 10 ) {
					diffCounter++;
				}
				a /= 10;
				b /= 10;
			}
			if ( a > 0 ) {
				while ( a > 0 ) {
					diffCounter++;
					a /= 10;
				}
			}
			if ( b > 0 ) {
				while ( b > 0 ) {
					diffCounter++;
					b /= 10;
				}
			}
		}
		return diffCounter;
	}

	/**
	 * Checks the best matches in the {@link #dictionary dictionary} for the
	 * given integer by calling on the {@link #countDifferences(int,int)
	 * countDifferences} method for each dictionary entries
	 * 
	 * @param inputNumber
	 *            integer to be checked against the dictionary
	 * @return an array of matches with the lowest differences
	 * @exception nullPointerException
	 *                when the {@link #dictionary dictionary} is empty
	 */
	public int[] findBestMatches( int inputNumber ) throws NullPointerException {
		if ( dictionary == null || dictionary.length < 1 ) {
			throw new NullPointerException( "Numeric Spell Checker dictionary array is empty" );
		}
		int[][] allMatches = new int[ dictionary.length ][];
		int bestMatchScore = String.valueOf( inputNumber ).trim().length();
		int bestMatchCounter = 0;
		/*
		 * Checks entire dictionary against inputNumber and put dictionary entry
		 * and resulting match score <diff> in the <allMatches> array.
		 */
		for ( int i = 0; i < dictionary.length; i++ ) {
			int diff = countDifferences( inputNumber, dictionary[ i ] );
			if ( diff < bestMatchScore ) {
				bestMatchScore = diff;
				bestMatchCounter = 0;
			}
			if ( diff == bestMatchScore ) {
				bestMatchCounter++;
			}
			allMatches[ i ] = new int[] { diff, dictionary[ i ] };
		}
		// Sort array by their diff value from low to high.
		Arrays.sort( allMatches, new Comparator<int[]>() {
			public int compare( int[] a, int[] b ) {
				return Integer.compare( a[ 0 ], b[ 0 ] );
			}
		} );
		// Create an array and add all the top (the lowest diff) matches to it
		int[] bestMatches = new int[ bestMatchCounter ];
		for ( int i = 0; i < bestMatches.length; i++ ) {
			bestMatches[ i ] = allMatches[ i ][ 1 ];
		}
		return bestMatches;
	}

	/**
	 * Main method driving the dictionary search
	 */
	public static void main( String[] args ) {
		NumericSpellChecker nsc = new NumericSpellChecker();
		nsc.createRandomDictionary( 1000 );
		for ( int i : nsc.dictionary ) {
			System.out.print( i + " | " );
		}
		System.out.println( "" );
		Scanner scanner = new Scanner( System.in );
		int y = 0;
		do {
			y = getNumber( scanner );
			if ( y != 0 ) {
				if ( nsc.exactMatch( y ) == true ) {
					System.out.println( "Exact match found!!" );
				} else {
					int[] bestMatches = nsc.findBestMatches( y );
					if ( bestMatches.length > 0 ) {
						System.out.println( "No exact matches found, closest matches are:" );
						for ( int best : bestMatches ) {
							System.out.println( best );
						}
					} else {
						System.out.println( "No exact or close matches found." );
					}
				}
			} else {
				System.out.println( "Exiting Dictionary search..." );
			}
		} while ( y != 0 );
		scanner.close();
	}

	/**
	 * Enquires the user for an interger input
	 * 
	 * @return the integer entered by the user
	 */
	public static int getNumber( Scanner scanner ) {

		System.out.print( "Enter an integer: " );
		while ( true ) {
			if ( scanner.hasNextInt() ) {
				int result = scanner.nextInt();
				scanner.nextLine();
				return result;
			} else {
				System.out.print( "\"" + scanner.nextLine() + "\" is not an integer. Try again: " );
			}
		}
	}
}