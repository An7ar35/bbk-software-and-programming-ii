/**
 * <b>BoneMoveTest</b><br/>
 * JUnit tests for the BoneMove class
 * 
 * @author Es Alwyn DAVISON <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package UnitTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import dominoes.Bone;
import dominoes.BoneMove;
import dominoes.BoneMove.Moves;

public class BoneMoveTest {
	private static final int LEFT_SIDE = 0;
	private static final int RIGHT_SIDE = 1;
	private static Bone b;
	private static int index;
	private static BoneMove bm;
	private static ArrayList<Moves> moves = new ArrayList<Moves>();

	@BeforeClass
	public static void beforeClass() {
		b = new Bone( 6, 2 );
		index = 5;
		bm = new BoneMove( b, index );
		moves.add( Moves.LEFT_NOFLIP );
		moves.add( Moves.LEFT_FLIP );
		moves.add( Moves.RIGHT_NOFLIP );
		moves.add( Moves.RIGHT_FLIP );
		bm.setMoves( moves );
	}

	@Test
	public void testGetBone() {
		Bone expected = new Bone( 6, 2);
		Bone returned = bm.getBone();
		assertTrue( returned.left() == expected.left() );
		assertTrue( returned.right() == expected.right() );
	}
	
	@Test
	public void testGetBoneIndex() {
		assertTrue( bm.getBoneIndex() == 5 );
	}

	@Test
	public void testSetMoves() {
		ArrayList<Moves> setMoves = new ArrayList<Moves>();
		setMoves.add( Moves.RIGHT_NOFLIP );
		setMoves.add( Moves.RIGHT_FLIP );
		setMoves.add( Moves.LEFT_NOFLIP );
		setMoves.add( Moves.LEFT_FLIP );
		bm.setMoves( setMoves );
		ArrayList<Moves> returnedMoves = bm.getAllMoves();
		for ( int i = 0; i < setMoves.size(); i++ ) {
			assertTrue( setMoves.get( i ) == returnedMoves.get( i ) );
		}
	}

	@Test
	public void testGetAllMoves() {
		ArrayList<Moves> expectedMoves = new ArrayList<Moves>();
		expectedMoves.add( Moves.LEFT_NOFLIP );
		expectedMoves.add( Moves.LEFT_FLIP );
		expectedMoves.add( Moves.RIGHT_NOFLIP );
		expectedMoves.add( Moves.RIGHT_FLIP );
		ArrayList<Moves> returnedMoves = bm.getAllMoves();
		for ( int i = 0; i < expectedMoves.size(); i++ ) {
			assertTrue( expectedMoves.get( i ) == returnedMoves.get( i ) );
		}
	}

	@Test
	public void testGetMoveForSide() {
		assertTrue( bm.getMoveForSide( LEFT_SIDE ) == Moves.LEFT_FLIP );
		assertTrue( bm.getMoveForSide( RIGHT_SIDE ) == Moves.RIGHT_FLIP );
	}

	@Test
	public void testGetMovesNumber() {
		assertTrue( bm.getMovesNumber() == 4 );
	}

	@Test
	public void testToString() {
		String expected = "Possible moves with [6|2] : LEFT_NOFLIP LEFT_FLIP RIGHT_NOFLIP RIGHT_FLIP";
		String actual = bm.toString();
		assertEquals( expected, actual );
	}

}
