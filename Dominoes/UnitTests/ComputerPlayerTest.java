/**
 * <b>ComputerPlayerTest</b><br/>
 * JUnit tests for the ComputerPlayer class
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package UnitTests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dominoes.Bone;
import dominoes.BoneYard;
import dominoes.CantPlayException;
import dominoes.InvalidPlayException;
import dominoes.Play;
import dominoes.Table;
import dominoes.players.ComputerPlayer;
import dominoes.players.DominoPlayer;

public class ComputerPlayerTest {
	private DominoPlayer dp;
	@SuppressWarnings( "unused" )
	private static final int LEFT_SIDE = 0;
	private static final int RIGHT_SIDE = 1;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		dp = new ComputerPlayer();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#ComputerPlayer()}.
	 * 
	 */
	@Test
	public void testComputerPlayer() {
		assertEquals( "Errors", dp.getName(), "Computer" );
		assertEquals( "Errors", dp.getPoints(), 0 );
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#newRound()}.
	 */
	@Test
	public void testNewRound() {
		BoneYard myYard = new BoneYard( 2 );
		dp.draw( myYard );
		dp.draw( myYard );
		dp.draw( myYard );
		dp.draw( myYard );
		// We had 4 draws so 4 entries
		assertEquals( "Error", dp.bonesInHand().length, 4 );
		dp.newRound();
		// After We calling the newRound it is 0 again
		assertEquals( "Error", dp.bonesInHand().length, 0 );
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#bonesInHand()}.
	 */
	@Test
	public void testBonesInHand() {
		Bone a = new Bone( 2, 3 );
		dp.takeBack( a );
		assertEquals( "Errors", dp.bonesInHand().length, 1 );
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#draw(dominoes.BoneYard)}. Draw -->
	 * When you transfer a bone from the BoneYard into your hand, this is called
	 * a "draw". In this Test Method is tested whenever a Player pick or better
	 * "draw" from the BoneYard (when the game starts) BoneYard then decreases
	 * and myHand increase. tested on 28 tiles by 6 dots
	 */
	@Test
	public void testDraw() {
		// variable needed for length
		int myTestBoneInHandLen = 0;
		int testBoneyardLen = 0;

		// total of 28 tiles possible
		BoneYard testBoneyard = new BoneYard( 6 );
		testBoneyardLen = testBoneyard.size();

		// my bone in hand are 0 at this point
		myTestBoneInHandLen = dp.bonesInHand().length;

		assertEquals( "Error BoneYard", testBoneyardLen, 28 );
		assertEquals( "Error InHands", myTestBoneInHandLen, 0 );

		// lets draw
		dp.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = dp.bonesInHand().length;

		// my hands increase by 1
		assertEquals( "Error InHands", myTestBoneInHandLen, 1 );
		// my BoneYard decrease by 1
		assertEquals( "Error BoneYard", testBoneyardLen, 27 );
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#draw(dominoes.BoneYard)}. Draw -->
	 * When you transfer a bone from the BoneYard into your hand, this is called
	 * a "draw". In this Test Method is tested whenever a Player pick or better
	 * "draw" from the BoneYard (when the game starts) BoneYard then decreases
	 * and myHand increase. tested on 3 tiles by 1 dots. This time we will draw
	 * 4 times in order to test that my Hand do not increase.
	 */
	@Test
	public void testDrawNull() {
		// variable needed for length
		int myTestBoneInHandLen = 0;
		int testBoneyardLen = 0;

		// total of 28 tiles possible
		BoneYard testBoneyard = new BoneYard( 1 );
		testBoneyardLen = testBoneyard.size();

		// my bone in hand are 0 at this point
		myTestBoneInHandLen = dp.bonesInHand().length;

		assertEquals( "Error BoneYard", testBoneyardLen, 3 );
		assertEquals( "Error InHands", myTestBoneInHandLen, 0 );

		dp.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = dp.bonesInHand().length;
		// my hands has 1 bone
		assertEquals( "Error InHands", myTestBoneInHandLen, 1 );
		// my BoneYard has 2 bones
		assertEquals( "Error BoneYard", testBoneyardLen, 2 );

		dp.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = dp.bonesInHand().length;
		// my hands has 2 bones
		assertEquals( "Error InHands", myTestBoneInHandLen, 2 );
		// my BoneYard has 1
		assertEquals( "Error BoneYard", testBoneyardLen, 1 );

		dp.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = dp.bonesInHand().length;
		// my hands has 3 bones
		assertEquals( "Error InHands", myTestBoneInHandLen, 3 );
		// my BoneYard has 0
		assertEquals( "Error BoneYard", testBoneyardLen, 0 );

		dp.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = dp.bonesInHand().length;
		// my hands should by 3 as the BoneYard is empty now
		assertEquals( "Error InHands", myTestBoneInHandLen, 3 );
		// my BoneYard is still 0
		assertEquals( "Error BoneYard", testBoneyardLen, 0 );
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#getName()}.
	 */
	@Test
	public void testGetName() {
		dp.setName( "Francis" );
		assertNotNull( dp.getName() );
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#getPoints()}.
	 */
	@Test
	public void testGetPoints() {
		for ( int i = 50; i < 1000; i += 50 ) {
			dp.setPoints( 50 );
			assertEquals( dp.getPoints(), i );
		}
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#makePlay(dominoes.Table)}.
	 * 
	 * @throws CantPlayException
	 */
	@Test( expected = CantPlayException.class )
	public void testMakePlayException() throws CantPlayException {
		dp.makePlay( new Table() );
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#makePlay(dominoes.Table)}. Testing
	 * 4 Different Playable Bones in Hand In this Test all 4 will be available
	 * Table 1|1 HumanPlayer has 1|1 / 1|1
	 */
	@Test
	public void testMakePlayEasy() {
		Table table = new Table();
		Bone b = new Bone( 1, 1 );
		Play p = new Play( b, 2 );
		try {
			table.play( p );
			Bone playableBone = new Bone( 1, 1 );
			dp.takeBack( playableBone );
			try {
				Play returned = dp.makePlay( table );
				// Checking that the bone has been played has same side
				assertEquals( ( returned.bone() ).right(), playableBone.right() );
				assertEquals( ( returned.bone() ).left(), playableBone.left() );
			} catch ( CantPlayException e ) {
				fail("no valid bones seen in the player's hand");
			}
		} catch ( InvalidPlayException e ) {
			fail("table did not accept the play for the origin bone");
		}
	}

	@Test( expected = CantPlayException.class )
	public void testMakeTestFalse() throws CantPlayException, InvalidPlayException {
		Table table = new Table();
		Bone b = new Bone( 4, 1 );
		Play p = new Play( b, 2 );
		table.play( p );
		Bone boneToPlay = new Bone( 2, 2 );
		dp.takeBack( boneToPlay );
		dp.makePlay( table );
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#makePlay(dominoes.Table)}. Testing
	 * 3 Different Playable Bones in Hand In this Test all 4 will be available
	 * Table 6|3 HumanPlayer has 6|3 / 3|4 / 4|3; THE SEQUENCE WILL FLIP BOTH
	 * SIDE
	 */
	@Test
	public void testMakePlayFlipBothSide() {
		Table table = new Table();
		Bone b = new Bone( 4, 6 );
		Play p = new Play( b, 2 );
		try {
			table.play( p );
			Bone playableBone = new Bone( 4, 3 );
			dp.takeBack( playableBone );
			try {
				Play returned = dp.makePlay( table );
				assertEquals( (returned.bone()).right(), playableBone.right() );
				assertEquals( (returned.bone()).left(), playableBone.left() );
				assertEquals( returned.end(), RIGHT_SIDE );
			} catch ( CantPlayException e ) {
				fail("no valid bones seen in the player's hand");
			}
		} catch ( InvalidPlayException e ) {
			fail("table did not accept the play for the origin bone");
		}
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#numInHand()}.
	 */
	@Test
	public void testNumInHand() {
		assertEquals( 0, dp.numInHand() );
		BoneYard by = new BoneYard( 6 );
		dp.draw( by );
		assertEquals( 1, dp.numInHand() );
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		String s = "Robin Hood";
		dp.setName( s );
		assertEquals( dp.getName(), s );
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#setPoints(int)}.
	 */
	@Test
	public void testSetPoints() {
		for ( int i = 1; i < 100; i++ ) {
			dp.setPoints( 1 );
			assertEquals( dp.getPoints(), i );
		}
	}

	/**
	 * Test method for
	 * {@link dominoes.players.ComputerPlayer#takeBack(dominoes.Bone)}.
	 */
	@Test
	public void testTakeBack() {
		Bone bone = new Bone( 9, 9 );
		dp.takeBack( bone );
		Bone[] inHand = dp.bonesInHand();
		boolean flag = false;
		for ( int i = 0; i < inHand.length; i++ ) {
			if ( inHand[ i ] == bone ) {
				flag = true;
			}
		}
		assertSame( flag, true );
	}

	/**
	 * Test method for {@link dominoes.players.ComputerPlayer#toString()}.
	 */
	@Test
	public void testToString() {
		String name = "Emperor Corniferous";
		dp.setName( name );
		assertEquals( dp.toString(), name );
	}

}
