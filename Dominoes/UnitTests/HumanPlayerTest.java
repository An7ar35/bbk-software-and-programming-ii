/**
 * <b>ComputerPlayerTest</b><br/>
 * JUnit tests for the ComputerPlayer class
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package UnitTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.*;

import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import dominoes.Bone;
import dominoes.BoneYard;
import dominoes.CantPlayException;
import dominoes.InvalidPlayException;
import dominoes.Play;
import dominoes.Table;
import dominoes.players.DominoPlayer;
import dominoes.players.HumanPlayer;

import org.junit.After;
import org.junit.AfterClass;

public class HumanPlayerTest {
	public static HumanPlayer p;
	public static Bone bone;
	public static DominoPlayer dp;
	public static Bone myBoneToTest;
	public static Bone myBoneToTestb;
	public static Bone myBoneToTestc;
	public static Table t;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		p = new HumanPlayer();
		t = new Table();
		myBoneToTest = new Bone( 5, 5 );
		myBoneToTestb = new Bone( 4, 5 );
		myBoneToTestc = new Bone( 5, 3 );
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#HumanPlayer()}.
	 */
	@Test
	public void testHumanPlayer() {
		assertEquals( "Errors", p.getName(), "Player" );
		assertEquals( "Errors", p.getPoints(), 0 );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#bonesInHand()}.
	 */
	@Test
	public void testBonesInHand() {
		Bone a = new Bone( 2, 3 );
		p.takeBack( a );
		assertEquals( "Errors", p.bonesInHand().length, 1 );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#draw(dominoes.BoneYard)}.
	 */
	@Test
	public void testDraw() {
		// variable needed for length
		int myTestBoneInHandLen = 0;
		int testBoneyardLen = 0;

		// total of 28 tiles possible
		BoneYard testBoneyard = new BoneYard( 6 );
		testBoneyardLen = testBoneyard.size();

		// my bone in hand are 0 at this point
		myTestBoneInHandLen = p.bonesInHand().length;

		assertEquals( "Error BoneYard", testBoneyardLen, 28 );
		assertEquals( "Error InHands", myTestBoneInHandLen, 0 );

		// lets draw
		p.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = p.bonesInHand().length;

		// my hands increase by 1
		assertEquals( "Error InHands", myTestBoneInHandLen, 1 );
		// my BoneYard decrease by 1
		assertEquals( "Error BoneYard", testBoneyardLen, 27 );
	}
	
	/**
	 * Test method for {@link dominoes.players.HumanPlayer#draw(dominoes.BoneYard)}.
	 */
	@Test
	public void testDrawNull() {
		int myTestBoneInHandLen = 0;
		int testBoneyardLen = 0;

		// total of 28 tiles possible
		BoneYard testBoneyard = new BoneYard( 1 );
		testBoneyardLen = testBoneyard.size();

		// my bone in hand are 0 at this point
		myTestBoneInHandLen = p.bonesInHand().length;

		assertEquals( "Error BoneYard", testBoneyardLen, 3 );
		assertEquals( "Error InHands", myTestBoneInHandLen, 0 );

		p.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = p.bonesInHand().length;
		// my hands has 1 bone
		assertEquals( "Error InHands", myTestBoneInHandLen, 1 );
		// my BoneYard has 2 bones
		assertEquals( "Error BoneYard", testBoneyardLen, 2 );

		p.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = p.bonesInHand().length;
		// my hands has 2 bones
		assertEquals( "Error InHands", myTestBoneInHandLen, 2 );
		// my BoneYard has 1
		assertEquals( "Error BoneYard", testBoneyardLen, 1 );

		p.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = p.bonesInHand().length;
		// my hands has 3 bones
		assertEquals( "Error InHands", myTestBoneInHandLen, 3 );
		// my BoneYard has 0
		assertEquals( "Error BoneYard", testBoneyardLen, 0 );

		p.draw( testBoneyard );
		testBoneyardLen = testBoneyard.size();
		myTestBoneInHandLen = p.bonesInHand().length;
		// my hands should by 3 as the BoneYard is empty now
		assertEquals( "Error InHands", myTestBoneInHandLen, 3 );
		// my BoneYard is still 0
		assertEquals( "Error BoneYard", testBoneyardLen, 0 );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#getName()}.
	 */
	@Test
	public void testGetName() {
		assertTrue( p.getName() == "Player" );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#getPoints()}.
	 */
	@Test
	public void testGetPoints() {
		assertTrue( p.getPoints() == 0 );
	}

	@Rule //Required for simulating a console input
	public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();	

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#makePlay(dominoes.Table)}.
	 */
	@Test
	public void testMakePlaySingleBone() {
		Table table = new Table();
		Bone originBone = new Bone( 1, 2 );
		Play tablePlay = new Play( originBone, 2 );
		try {
			table.play( tablePlay );
			Bone unplayable = new Bone( 6, 6 );
			p.takeBack( unplayable );
			unplayable = new Bone( 5, 3 );
			p.takeBack( unplayable );
			Bone playable = new Bone( 2, 4 );
			p.takeBack( playable );
			assertEquals( "Wrong number of bones in player's hand", p.numInHand(), 3 );

			// Using the "System Rules" library to mock a console input stream
			// there is only one Bone which is Playable
			systemInMock.provideText( "1" );

			Play returnedPlay = p.makePlay( table );
			assertEquals( "Unexpected left  value from the returned bone", playable.left(), ( returnedPlay.bone() ).left() );
			assertEquals( "Unexpected right value from the returned bone", playable.right(), ( returnedPlay.bone() ).right() );

			// From 3 now we have 2 bones in numInHand
			assertEquals( "Wrong number of bones in player's hand", p.numInHand(), 2 );

		} catch ( InvalidPlayException e ) {
			fail( "table did not accept the play for the origin bone" );
		} catch ( CantPlayException e ) {
			fail( "no valid bones seen in the player's hand" );
		}
	}	
	
	/**
	 * Test method for {@link dominoes.players.HumanPlayer#makePlay(dominoes.Table)}.
	 */
	@Test
	public void testMakePlayMultipleBones() {
		Table table = new Table();
		Bone originBone = new Bone( 1, 6 );
		Play tablePlay = new Play( originBone, 2 );
		try {
			table.play( tablePlay );
			Bone playable = new Bone( 6, 6 );
			p.takeBack( playable );
			Bone unplayable = new Bone( 5, 3 );
			p.takeBack( unplayable );
			Bone playable2 = new Bone( 1, 1 );
			p.takeBack( playable2 );

			assertEquals( "Wrong number of bones in player's hand", p.numInHand(), 3 );

			// Using the "System Rules" library to mock a console input stream
			systemInMock.provideText( "1" );

			// java.lang.AssertionError: Unexpected left value from the returned
			// bone expected:<6> but was:<1>
			Play returnedPlay = p.makePlay( table );

			int rLeft = ( returnedPlay.bone() ).left();
			int rRight = ( returnedPlay.bone() ).right();
			boolean expectedReturn = false;
			if ( ( playable.left() == rLeft && playable.right() == rRight ) || ( playable2.left() == rLeft && playable2.right() == rRight ) ) {
				expectedReturn = true;
			}
			assertEquals( "Unexpected end value(s) from returned bone", true, expectedReturn );
			assertEquals( "Wrong number of bones in player's hand", p.numInHand(), 2 );
		} catch ( InvalidPlayException e ) {
			fail( "table did not accept the play for the origin bone" );
		} catch ( CantPlayException e ) {
			fail( "no valid bones were seen in the player's hand" );
		}
	}	
	
	/**
	 * Test method for {@link dominoes.players.HumanPlayer#makePlay(dominoes.Table)}.
	 */	
	@Test( expected = CantPlayException.class )
	public void testMakePlayFail() throws CantPlayException, InvalidPlayException {
		Table table = new Table();
		Bone originBone = new Bone( 4, 2 );
		Play tablePlay = new Play( originBone, 2 );
		try {
			table.play( tablePlay );
			Bone unplayable = new Bone( 6, 6 );
			p.takeBack( unplayable );
			Bone unplayable2 = new Bone( 5, 3 );
			p.takeBack( unplayable2 );
			Bone unplayable3 = new Bone( 1, 1 );
			p.takeBack( unplayable3 );

			assertEquals( "Wrong number of bones in player's hand", p.numInHand(), 3 );
			@SuppressWarnings( "unused" )
			Play returnedPlay = p.makePlay( table );
		} catch ( InvalidPlayException e ) {
			fail( "table did not accept the play for the origin bone" );
		}
	}	

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#newRound()}.
	 */
	@Test
	public void testNewRound() {
		BoneYard myYard = new BoneYard( 2 );
		p.draw( myYard );
		p.draw( myYard );
		p.draw( myYard );
		p.draw( myYard );
		assertEquals( "Error", p.bonesInHand().length, 4 );
		p.newRound();
		assertEquals( "Error", p.bonesInHand().length, 0 );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#numInHand()}.
	 */
	@Test
	public void testNumInHand() {
		assertEquals( 0, p.numInHand() );
		BoneYard by = new BoneYard( 6 );
		p.draw( by );
		assertEquals( 1, p.numInHand() );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		p.setName( "Franco" );
		assertTrue( p.getName() == "Franco" );
	}
	
	/**
	 * Test method for {@link dominoes.players.HumanPlayer#toString(java.lang.String)}.
	 */
	@Test
	public void testToString() {
		p.setName( "Esmond" );
		assertTrue( p.toString() == "Esmond" );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#setPoints(int)}.
	 */
	@Test
	public void testSetPoints() {
		p.setPoints( 25 );
		assertTrue( p.getPoints() == 25 );
		p.setPoints( 25 );
		assertTrue( p.getPoints() == 50 );
		p.setPoints( 0 );
		assertTrue( p.getPoints() == 50 );
		p.setPoints( 3 );
		assertTrue( p.getPoints() == 53 );
	}

	/**
	 * Test method for {@link dominoes.players.HumanPlayer#takeBack(dominoes.Bone)}.
	 */
	@Test
	public void testTakeBack() {
		Bone bone = new Bone( 9, 9 );
		p.takeBack( bone );
		Bone[] inHand = p.bonesInHand();
		boolean flag = false;
		for ( int i = 0; i < inHand.length; i++ ) {
			if ( inHand[ i ] == bone ) {
				flag = true;
			}
		}
		assertSame( flag, true );
	}

}
