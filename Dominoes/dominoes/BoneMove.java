/**
 * <b>Bone Move</b><br/>
 * Object that records the possible moves on a given table by a Bone
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package dominoes;

import java.util.ArrayList;

public class BoneMove {
	public enum Moves {
		LEFT_NOFLIP, LEFT_FLIP, RIGHT_NOFLIP, RIGHT_FLIP
	};

	private static final int LEFT = 0;
	private static final int RIGHT = 1;
	private Bone bone;
	private int itsBoneIndex;
	private ArrayList<Moves> itsListOfMoves = new ArrayList<Moves>();

	/**
	 * Constructor for the BoneMove
	 * 
	 * @param bone
	 *            Bone
	 * @param index
	 *            position where the bone resides in its Vector
	 */
	public BoneMove( Bone bone, int index ) {
		this.itsBoneIndex = index;
		this.bone = bone;
	}

	/**
	 * Gives the bone
	 * 
	 * @return the bone as a Bone object
	 */
	public Bone getBone() {
		return this.bone;
	}

	/**
	 * Gives the total value of the bone back by using the
	 * {@link #Dominoes.Bone.weight() weight} method
	 * 
	 * @return the weight of the bone
	 */
	public int getBoneWeight() {
		return bone.weight();
	}

	/**
	 * Gives the index for the bone
	 * 
	 * @return the itsBoneIndex
	 */
	public int getBoneIndex() {
		return this.itsBoneIndex;
	}

	/**
	 * Sets moves
	 * 
	 * @param moves
	 *            an ArrayList of Moves
	 */
	public void setMoves( ArrayList<Moves> moves ) {
		this.itsListOfMoves = moves;
	}

	/**
	 * Gets the list of all possible moves
	 * 
	 * @return the list of possible moves
	 */
	public ArrayList<Moves> getAllMoves() {
		return this.itsListOfMoves;
	}

	/**
	 * Gets the move for a specified side of the table
	 * 
	 * @param side
	 *            the side of the table for which the moves are required
	 * @return the move associated with the given side
	 */
	public Moves getMoveForSide( int side ) {
		Moves move = null;
		if ( side == LEFT ) {
			for ( int i = 0; i < this.itsListOfMoves.size(); i++ ) {
				if ( this.itsListOfMoves.get( i ) == Moves.LEFT_NOFLIP || this.itsListOfMoves.get( i ) == Moves.LEFT_FLIP ) {
					move = this.itsListOfMoves.get( i );
				}
			}
		} else if ( side == RIGHT ) {
			for ( int i = 0; i < this.itsListOfMoves.size(); i++ ) {
				if ( this.itsListOfMoves.get( i ) == Moves.RIGHT_NOFLIP || this.itsListOfMoves.get( i ) == Moves.RIGHT_FLIP ) {
					move = this.itsListOfMoves.get( i );
				}
			}
		}
		return move;
	}

	/**
	 * Gives the number of possible moves
	 * 
	 * @return size of the itsListOfMoves ListArray
	 */
	public int getMovesNumber() {
		return this.itsListOfMoves.size();
	}

	@Override
	public String toString() {
		String possibleMoves = "";
		for ( Moves m : this.itsListOfMoves ) {
			possibleMoves += " " + m;
		}
		return "Possible moves with [" + bone.left() + "|" + bone.right() + "] :" + possibleMoves;
	}

}
