/**
 * <b>Driver</b><br/>
 * Game driver for the Dominoes
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package dominoes;

import java.util.Scanner;

import dominoes.players.ComputerPlayer;
import dominoes.players.DominoPlayer;
import dominoes.players.HumanPlayer;

public class Driver {
	private static int gamePoints = 100;

	/**
	 * ASCII graphics for the start of the application
	 */
	public static void showTitleScreen() {
		System.out.println( "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" );
		System.out.println( "OOO           OOO           OOO  OOOOOOOO  OOO      OOOO  OOOOOO  OOOO          OOOO        OOOO       OOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO   OOOOOO   OOOOO  OOOOOO  OOOOOO  OOOO  OOOOOO  OOOO  OOOOOOOOOO  OOOOOOOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO  O OOOO O  OOOOO  OOOOOO   OOOOO  OOOO  OOOOOO  OOOO  OOOOOOOOOO  OOOOOOOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO  OO OO OO  OOOOO  OOOOOO  O OOOO  OOOO  OOOOOO  OOOO  OOOOOOOOOO  OOOOOOOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO  OOO  OOO  OOOOO  OOOOOO  OO OOO  OOOO  OOOOOO  OOOO     OOOOOOO       OOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO  OOOOOOOO  OOOOO  OOOOOO  OOO OO  OOOO  OOOOOO  OOOO  OOOOOOOOOOOOOOO  OOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO  OOOOOOOO  OOOOO  OOOOOO  OOOO O  OOOO  OOOOOO  OOOO  OOOOOOOOOOOOOOO  OOO" );
		System.out.println( "OOOOO OOOOOO  OOO  OOOOOOO  OOO  OOOOOOOO  OOOOO  OOOOOO  OOOOO   OOOO  OOOOOO  OOOO  OOOOOOOOOOOOOOO  OOO" );
		System.out.println( "000           OOO           OOO  OOOOOOOO  OOO      OOOO  OOOOOO  OOOO          OOOO         OOO       OOO" );
		System.out.println( "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" );
		System.out.println( "*************UI & Player implementation by Es DAVISON and Testing by Franco CARDINALI , 2013**************\n" );
	}

	/**
	 * Main method driving the game through a console menu system
	 * 
	 * @param args
	 *            command arguments (NOT used)
	 */
	public static void main( String[] args ) {
		showTitleScreen();
		boolean inGame = true;
		boolean validName;
		String P1name;
		String P2name;
		Scanner scan = new Scanner( System.in );
		DominoPlayer player1;
		DominoPlayer player2;
		while ( inGame == true ) {
			System.out.println( ".---------------------------------------. " );
			System.out.println( "|           DOMINOES MAIN MENU          | " );
			System.out.println( ".---------------------------------------. " );
			System.out.println( "|  Please choose your option:           | " );
			System.out.println( "|                                       | " );
			System.out.println( "|  [a] PC vs PC                         | " );
			System.out.println( "|  [b] Human vs PC                      | " );
			System.out.println( "|  [c] Human vs Human                   | " );
			System.out.println( "|  [p] Set game points (Currently: " + getGamePoints() + ") | " );
			System.out.println( "|  [x] Exit                             | " );
			System.out.println( "|                                       | " );
			System.out.println( "'---------------------------------------' " );
			System.out.print( " >> " );
			String menuChoice = scan.nextLine();
			switch ( menuChoice.toLowerCase() ) {
				case "a":
					player1 = new ComputerPlayer();
					player2 = new ComputerPlayer();
					player1.setName( "WOPR" );
					player2.setName( "Shodan" );
					startGame( scan, player1, player2 );
					break;
				case "b":
					validName = false;
					P1name = "Human Player";
					while ( validName == false ) {
						System.out.print( "Would you like to name the player? (Y/N) " );
						String binaryChoice = scan.nextLine();
						switch ( binaryChoice.toLowerCase() ) {
							case "y":
								P1name = namer( scan, 1 );
								validName = true;
								break;
							case "n":
								System.out.print( "Using defaults. You are \'" + P1name + "\'.\n" );
								validName = true;
								break;
							default:
								System.out.print( "Invalid choice, try again... \n" );
								break;
						}
					}
					player1 = new HumanPlayer(); // Change to HumanPlayer
					player2 = new ComputerPlayer();
					player1.setName( P1name );
					player2.setName( "Shodan" );
					startGame( scan, player1, player2 );
					break;
				case "c":
					validName = false;
					P1name = "Human Player 1";
					P2name = "Human Player 2";
					while ( validName == false ) {
						System.out.print( "Would you like to name the players? (Y/N) " );
						String binaryChoice = scan.nextLine();
						switch ( binaryChoice.toLowerCase() ) {
							case "y":
								P1name = namer( scan, 1 );
								P2name = namer( scan, 2 );
								validName = true;
								break;
							case "n":
								System.out.print( "Using defaults. Player 1 is \'" + P1name + "\' and Player 2 is \'" + P2name + "\'." );
								validName = true;
								break;
							default:
								System.out.print( "Invalid choice, try again... \n" );
								break;
						}
					}
					player1 = new HumanPlayer();
					player2 = new HumanPlayer();
					player1.setName( P1name );
					player2.setName( P2name );
					startGame( scan, player1, player2 );
					break;
				case "p":
					setGamePoints( scan );
					break;
				case "x":
					inGame = false;
					break;
				default:
					System.out.print( "\nInvalid choice, try again... \n" );
					break;
			}
		}
		scan.close();
		System.out.println( "exiting game.... Goodbye." );
	}

	/**
	 * Initiates the GameUI and the Dominoes game engine and calls the
	 * {@link #printFinalScores(DominoPlayer, DominoPlayer ) printFinalScores}
	 * method
	 * 
	 * @param player1
	 *            first instance of DominoPlayer
	 * @param player2
	 *            second instance of DominoPlayer
	 */
	public static void startGame( Scanner scan, DominoPlayer player1, DominoPlayer player2 ) {
		GameUI ui = new GameUI();
		Dominoes game = new Dominoes( ui, player1, player2, getGamePoints(), 6 );
		game.play();
		printFinalScores( player1, player2 );
	}

	/**
	 * Displays the final scores for both players
	 * 
	 * @param player1
	 *            first instance of DominoPlayer
	 * @param player2
	 *            second instance of DominoPlayer
	 */
	public static void printFinalScores( DominoPlayer player1, DominoPlayer player2 ) {
		System.out.println( "_________________________________________________________________________________________________" );
		System.out.println( "FINAL SCORES | " + player1.getName() + " : " + player1.getPoints() + " | " + player2.getName() + " : " + player2.getPoints() + " |" );
		if ( player1.getPoints() == player2.getPoints() ) {
			System.out.println( "\t===== GAME TIE =====" );
		} else if ( player1.getPoints() > player2.getPoints() ) {
			System.out.println( "\t===== " + player1.getName() + " WINS!! =====" );
		} else {
			System.out.println( "\t===== " + player2.getName() + " WINS!! =====" );
		}
		System.out.println( "_________________________________________________________________________________________________" );
	}

	/**
	 * Deals with the String input for the names of the player(s)
	 * 
	 * @param scan
	 *            Scanner instance
	 * @param player
	 *            player number (e.g.: '1' for Player 1)
	 * @return the name as a String
	 */
	public static String namer( Scanner scan, int player ) {
		boolean validName = false;
		String name = "";
		System.out.print( "Enter name for Player " + player + ": " );
		name = scan.nextLine();
		while ( validName == false ) {
			if ( name.length() == 0 ) {
				System.out.print( "Nothing entered, try again with more zeel... : " );
				name = scan.nextLine();
			} else if ( name.length() > 15 ) {
				System.out.print( "Name is too long ( 15 chars max ). Please try again: " );
			} else {
				System.out.print( "Player " + player + "\'s name set to " + name + ". Confirm? (Y/N) " );
				String binaryChoice = scan.nextLine();
				switch ( binaryChoice.toLowerCase() ) {
					case "y":
						validName = true;
						break;
					case "n":
						System.out.print( "Enter name for Player" + player + ": " );
						name = scan.nextLine();
						break;
					default:
						System.out.print( "Invalid choice. (Y) for Yes, (N) for No.\n" );
				}
			}
		}
		return name;
	}

	/**
	 * Sets the number of points to win a game
	 * 
	 * @param scan
	 *            Scanner instance
	 */
	public static void setGamePoints( Scanner scan ) {
		boolean isValid = false;
		int choice = getGamePoints();
		do {
			System.out.print( "Number of points to win the game (100-999): " );
			String input = scan.nextLine();
			if ( isInteger( input ) == true ) {
				choice = Integer.parseInt( input );
				if ( choice >= 100 && choice < 1000 ) {
					gamePoints = choice;
					isValid = true;
				} else {
					System.out.println( "Invalid number of points." );
				}
			} else {
				System.out.println( "Invalid input." );
			}
		} while ( isValid == false );
	}

	/**
	 * Gets the number of game points currently set to win a game
	 * 
	 * @return the gamePoints as an integer
	 */
	private static int getGamePoints() {
		return gamePoints;
	}

	/**
	 * Checks if a string is an Integer
	 * 
	 * @param s
	 *            String to check
	 * @return boolean result
	 */
	private static boolean isInteger( String s ) {
		try {
			Integer.parseInt( s );
			return true;
		} catch ( Exception e ) {
			return false;
		}
	}
}
