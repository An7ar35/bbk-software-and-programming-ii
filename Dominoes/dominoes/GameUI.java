/**
 * <b>Game UI</b><br/>
 * DominoeUI implementation
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package dominoes;

import dominoes.players.DominoPlayer;

public class GameUI implements DominoUI {
	private int round = 1;

	@Override
	public void display( DominoPlayer[] players, Table table, BoneYard boneyard ) {
		// Player scores
		System.out.println( "\n| ROUND " + getRound() + " | SCORES � " + players[ 0 ] + " : " + players[ 0 ].getPoints() + " / " + players[ 1 ] + " : " + players[ 1 ].getPoints() + " |" );
		// Boneyard
		System.out.println( boneyard.size() + " bones in boneyard." );
		// Table
		System.out.println( "=======================================================================================" );
		System.out.println( "----TABLE------------------------------------------------------------------------------" );
		Bone[] layout = table.layout();
		for ( Bone x : layout ) {
			System.out.print( "[" + x.left() + "|" + x.right() + "]" );
		}
		System.out.println( "\n---------------------------------------------------------------------------------------" );
		System.out.println( "=======================================================================================" );
		System.out.println( "\n" );
	}

	@Override
	public void displayInvalidPlay( DominoPlayer player ) {
		System.out.println( player + " made an invalid move..." );
	}

	@Override
	public void displayRoundWinner( DominoPlayer player ) {
		System.out.println( "*************** Round " + getRound() + " Winner: " + player + " ***************\n\n" );
		nextRound();
	}

	/**
	 * Progresses to next round
	 */
	private void nextRound() {
		this.round++;
	}

	/**
	 * Gets the current round
	 * 
	 * @return the current round as an integer
	 */
	private int getRound() {
		return this.round;
	}

}
