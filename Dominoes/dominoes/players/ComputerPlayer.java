/**
 * <b>ComputerPlayer</b><br/>
 * DominoePlayer implementation for an automated Computer player
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package dominoes.players;

import dominoes.Bone;
import dominoes.BoneMove;
import dominoes.BoneYard;
import dominoes.CantPlayException;
import dominoes.Play;
import dominoes.Table;
import dominoes.BoneMove.Moves;

import java.util.ArrayList;
import java.util.Vector;
import java.util.Random;

public class ComputerPlayer implements DominoPlayer {
	private static final int LEFT_SIDE = 0;
	private static final int RIGHT_SIDE = 1;
	private String itsName;
	private int itsPoints;
	private Vector<Bone> itsBonesInHand = new Vector<Bone>();

	public ComputerPlayer() {
		this.itsName = "Computer";
		this.itsPoints = 0;
	}

	@Override
	public Bone[] bonesInHand() {
		Bone[] hand = new Bone[ itsBonesInHand.size() ];
		itsBonesInHand.toArray( hand );
		return hand;
	}

	@Override
	public void draw( BoneYard boneyard ) {
		Bone b = boneyard.draw();
		if ( b != null ) {
			itsBonesInHand.add( b );
		}
	}

	@Override
	public String getName() {
		return this.itsName;
	}

	@Override
	public int getPoints() {
		return this.itsPoints;
	}

	@Override
	public Play makePlay( Table table ) throws CantPlayException {
		System.out.println( this.getName() + "'s turn.." );
		ArrayList<BoneMove> playableBones = this.getPlayableBones( table );
		int choices = playableBones.size();

		if ( choices > 0 ) {
			Play play;
			int playableBonesIndex = 0;

			// Looking for the highest weight if multiple choices of bones
			if ( choices > 1 ) {
				int highestWeight = 0;
				for ( int i = 0; i < playableBones.size(); i++ ) {
					int weight = playableBones.get( i ).getBoneWeight();
					if ( weight > highestWeight ) {
						playableBonesIndex = i;
					}
				}
			}

			int indexOfBone = playableBones.get( playableBonesIndex ).getBoneIndex();
			ArrayList<Moves> listOfMoves = playableBones.get( playableBonesIndex ).getAllMoves();
			Bone bone = this.itsBonesInHand.get( indexOfBone );

			Moves move = listOfMoves.get( 0 );
			if ( playableBones.get( playableBonesIndex ).getMovesNumber() > 1 ) {
				int coinFlip = coinFlip();
				switch ( coinFlip ) {
					case LEFT_SIDE:
						move = playableBones.get( playableBonesIndex ).getMoveForSide( LEFT_SIDE );
						break;
					case RIGHT_SIDE:
						move = playableBones.get( playableBonesIndex ).getMoveForSide( RIGHT_SIDE );
						break;
					default:
						throw new CantPlayException();
				}
			}

			switch ( move ) {
				case LEFT_NOFLIP:
					play = new Play( bone, LEFT_SIDE );
					break;
				case LEFT_FLIP:
					bone.flip();
					play = new Play( bone, LEFT_SIDE );
					break;
				case RIGHT_NOFLIP:
					play = new Play( bone, RIGHT_SIDE );
					break;
				case RIGHT_FLIP:
					bone.flip();
					play = new Play( bone, RIGHT_SIDE );
					break;
				default:
					throw new CantPlayException( "Move returned from BoneMove is not a valid move" );
			}

			this.itsBonesInHand.remove( indexOfBone );
			return play;
		}

		System.out.println( this.getName() + " can't play..." );
		throw new CantPlayException();
	}

	@Override
	public void newRound() {
		itsBonesInHand.removeAllElements();
	}

	@Override
	public int numInHand() {
		return this.itsBonesInHand.size();
	}

	@Override
	public void setName( String name ) {
		this.itsName = name;
	}

	@Override
	public void setPoints( int points ) {
		this.itsPoints += points;
	}

	@Override
	public void takeBack( Bone bone ) {
		this.itsBonesInHand.add( bone );
	}

	@Override
	public String toString() {
		return itsName;
	}

	/**
	 * Checks how many playable bones are in the player's hand and indexes their
	 * positions in the {@link #itsBonesInHand itsBonesInHand} into a Vector
	 * 
	 * @param table
	 *            the table where players play their bones
	 * @return playableBones ArrayList of BoneMove objects
	 */
	private ArrayList<BoneMove> getPlayableBones( Table table ) {
		ArrayList<BoneMove> playableBones = new ArrayList<BoneMove>();
		Bone bone;
		for ( int i = 0; i < this.numInHand(); i++ ) {
			ArrayList<Moves> moves = new ArrayList<Moves>();
			bone = this.itsBonesInHand.get( i );
			if ( bone.left() == bone.right() ) {
				if ( bone.left() == table.right() ) {
					moves.add( Moves.RIGHT_NOFLIP );
				}
				if ( bone.right() == table.left() ) {
					moves.add( Moves.LEFT_NOFLIP );
				}
			} else {
				if ( bone.left() == table.left() ) {
					moves.add( Moves.LEFT_FLIP );
				}
				if ( bone.left() == table.right() ) {
					moves.add( Moves.RIGHT_NOFLIP );
				}
				if ( bone.right() == table.left() ) {
					moves.add( Moves.LEFT_NOFLIP );
				}
				if ( bone.right() == table.right() ) {
					moves.add( Moves.RIGHT_FLIP );
				}
			}
			if ( moves.size() > 0 ) {
				playableBones.add( new BoneMove( bone, i ) );
				int j = playableBones.size() - 1;
				playableBones.get( j ).setMoves( moves );
			}
		}
		return playableBones;
	}

	/**
	 * Coin Flip method
	 * 
	 * @return the result of the coin flip as an integer (0/1)
	 */
	private int coinFlip() {
		Random random = new Random();
		int coinFlip = random.nextInt( 2 );
		return coinFlip;
	}
}
