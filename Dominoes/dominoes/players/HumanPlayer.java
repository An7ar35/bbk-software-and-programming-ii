/**
 * <b>HumanPlayer</b><br/>
 * DominoePlayer implementation for a human player
 * 
 * @author Es A. DAVISON & Franco Cardinali <br/>
 *         BSc Computing <br/>
 *         Due Sunday, 16 February 2014
 * @version 1.0
 */

package dominoes.players;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import dominoes.Bone;
import dominoes.BoneMove;
import dominoes.BoneMove.Moves;
import dominoes.BoneYard;
import dominoes.CantPlayException;
import dominoes.Play;
import dominoes.Table;

public class HumanPlayer implements DominoPlayer {
	private Scanner scan = new Scanner( System.in );
	private static final int LEFT_SIDE = 0;
	private static final int RIGHT_SIDE = 1;
	private String itsName;
	private int itsPoints;
	private Vector<Bone> itsBonesInHand = new Vector<Bone>();

	// private boolean inGameFlag = true;

	public HumanPlayer() {
		this.itsName = "Player";
		this.itsPoints = 0;
	}

	@Override
	public Bone[] bonesInHand() {
		Bone[] hand = new Bone[ this.numInHand() ];
		this.itsBonesInHand.toArray( hand );
		return hand;
	}

	@Override
	public void draw( BoneYard boneyard ) {
		Bone b = boneyard.draw();
		if ( b != null ) {
			this.itsBonesInHand.add( b );
		}
	}

	@Override
	public String getName() {
		return this.itsName;
	}

	@Override
	public int getPoints() {
		return this.itsPoints;
	}

	@Override
	public Play makePlay( Table table ) throws CantPlayException {
		System.out.println( this.getName() + "'s turn.." );
		boolean isValidSelection;
		System.out.println( "----All bones in your hand--------------------------------------------------------------" );
		this.displayPlayerHand();
		System.out.println( "\n----Playable bones----------------------------------------------------------------------" );
		ArrayList<BoneMove> playableBones = this.getPlayableBones( table );
		this.displayPlayableBones( playableBones );
		System.out.println( "\n----------------------------------------------------------------------------------------" );

		if ( playableBones.size() > 0 ) {
			int choice = 0;
			isValidSelection = false;
			do {
				System.out.print( "Type the number of the Bone you wish to play (1-" + playableBones.size() + "): " );
				String input = scan.nextLine();
				if ( isInteger( input ) == true ) {
					choice = Integer.parseInt( input ) - 1;
					if ( choice >= 0 && choice < playableBones.size() ) {
						isValidSelection = true;
					} else {
						System.out.println( "<" + ( choice + 1 ) + "> is not a possible choice." );
					}
				} else {
					System.out.println( "Invalid input." );
				}
			} while ( isValidSelection == false );
			Play play;
			int indexOfBone = playableBones.get( choice ).getBoneIndex();
			Bone bone = this.itsBonesInHand.get( indexOfBone );
			ArrayList<Moves> listOfMoves = playableBones.get( choice ).getAllMoves();
			Moves move = listOfMoves.get( 0 );

			if ( playableBones.get( choice ).getMovesNumber() > 1 ) {
				isValidSelection = false;
				do {
					System.out.println( "Which end? (L/R): " );
					String input = scan.nextLine();
					switch ( input.toLowerCase() ) {
						case "l":
							move = playableBones.get( choice ).getMoveForSide( LEFT_SIDE );
							isValidSelection = true;
							break;
						case "r":
							move = playableBones.get( choice ).getMoveForSide( RIGHT_SIDE );
							isValidSelection = true;
							break;
						default:
							System.out.print( "Invalid choice, try again... \n" );
							break;
					}
				} while ( isValidSelection == false );
			}

			switch ( move ) {
				case LEFT_NOFLIP:
					play = new Play( bone, LEFT_SIDE );
					break;
				case LEFT_FLIP:
					bone.flip();
					play = new Play( bone, LEFT_SIDE );
					break;
				case RIGHT_NOFLIP:
					play = new Play( bone, RIGHT_SIDE );
					break;
				case RIGHT_FLIP:
					bone.flip();
					play = new Play( bone, RIGHT_SIDE );
					break;
				default:
					throw new CantPlayException( "Move returned from BoneMove is not a valid move" );
			}

			this.itsBonesInHand.remove( indexOfBone );
			return play;
		}
		System.out.println( this.getName() + " can't play..." );
		throw new CantPlayException();
	}

	@Override
	public void newRound() {
		this.itsBonesInHand.removeAllElements();
	}

	@Override
	public int numInHand() {
		return this.itsBonesInHand.size();
	}

	@Override
	public void setName( String name ) {
		this.itsName = name;
	}

	@Override
	public void setPoints( int points ) {
		this.itsPoints += points;
	}

	@Override
	public void takeBack( Bone bone ) {
		this.itsBonesInHand.add( bone );
	}

	@Override
	public String toString() {
		return this.itsName;
	}

	/**
	 * Checks if a string is an Integer
	 * 
	 * @param s
	 *            String to check
	 * @return boolean result
	 */
	private static boolean isInteger( String s ) {
		try {
			Integer.parseInt( s );
			return true;
		} catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * Checks how many playable bones are in the player's hand and indexes their
	 * positions in the {@link #itsBonesInHand itsBonesInHand} into a Vector
	 * 
	 * @param table
	 *            the table where players play their bones
	 * @return playableBones ArrayList of BoneMove objects
	 */
	private ArrayList<BoneMove> getPlayableBones( Table table ) {
		ArrayList<BoneMove> playableBones = new ArrayList<BoneMove>();
		Bone bone;
		for ( int i = 0; i < this.numInHand(); i++ ) {
			ArrayList<Moves> moves = new ArrayList<Moves>();
			bone = this.itsBonesInHand.get( i );
			if ( bone.left() == bone.right() ) {
				if ( bone.left() == table.right() ) {
					moves.add( Moves.RIGHT_NOFLIP );
				}
				if ( bone.right() == table.left() ) {
					moves.add( Moves.LEFT_NOFLIP );
				}
			} else {
				if ( bone.left() == table.left() ) {
					moves.add( Moves.LEFT_FLIP );
				}
				if ( bone.left() == table.right() ) {
					moves.add( Moves.RIGHT_NOFLIP );
				}
				if ( bone.right() == table.left() ) {
					moves.add( Moves.LEFT_NOFLIP );
				}
				if ( bone.right() == table.right() ) {
					moves.add( Moves.RIGHT_FLIP );
				}
			}
			if ( moves.size() > 0 ) {
				playableBones.add( new BoneMove( bone, i ) );
				int j = playableBones.size() - 1;
				playableBones.get( j ).setMoves( moves );
				// System.out.println( playableBones.get( j ) );
			}
		}
		return playableBones;
	}

	/**
	 * Prints out the bones in the player's hand on the console screen
	 */
	private void displayPlayerHand() {
		Bone[] hand = this.bonesInHand();
		for ( Bone bone : hand ) {
			System.out.print( "[" + bone.left() + "|" + bone.right() + "]" );
		}
	}

	/**
	 * Prints out the bones that are playable out of the player's hand on the
	 * console screen
	 * 
	 * @param playableBones
	 *            ArrayList of BoneMove objects that hold the position in
	 *            {@link #itsBonesInHand itsBonesInHand} of the bones that are
	 *            playable (looked up with {@link #bonesInHand() bonesInHand})
	 */
	private void displayPlayableBones( ArrayList<BoneMove> playableBones ) {
		Bone[] hand = this.bonesInHand();
		Bone bone;
		int playable = playableBones.size();
		if ( playable > 0 ) {
			for ( int i = 0; i < playable; i++ ) {
				int index = playableBones.get( i ).getBoneIndex();
				bone = hand[ index ];
				System.out.print( "[" + bone.left() + "|" + bone.right() + "]" );
			}
			System.out.println( "" );
			for ( int i = 0; i < playableBones.size(); i++ ) {
				System.out.print( " <" + ( i + 1 ) + "> " );
			}
		} else {
			System.out.println( "No bones playable... :'( " );
		}
	}
}
